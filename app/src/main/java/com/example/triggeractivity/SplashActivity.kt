package com.example.triggeractivity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import kotlinx.android.synthetic.main.settings_splash_layout.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_splash_layout)

        val btnGo = this.btnGo as Button

        // On button click, save name and open main activity
        btnGo.setOnClickListener {
            // Explicit intent: open MainActivity in THIS app
            val intent = Intent(this, MainActivity::class.java)
            // Send the entered name to the main activity
            intent.putExtra("USER_ID", txtName.text.toString())
            startActivity(intent)
        }
    }

}