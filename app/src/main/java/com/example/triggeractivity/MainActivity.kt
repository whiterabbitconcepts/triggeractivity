package com.example.triggeractivity

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get the intent that started this and get the entered name
        val intent: Intent = getIntent()
        val name = intent.getStringExtra("USER_ID")

        // Send name to the TextView
        txtShowName.text = "Hi ${name}!"

        // Set image uri and update image
        val image = img as ImageView
        val uri: Uri = Uri.parse("android.resource://com.example.triggeractivity/" + R.drawable.ic_android_black_24dp)
        image.setImageURI(uri)

        // Add image click listener, to set up intent to open camera
        image.setOnClickListener {
            // Implicit intent: open camera app
            val implicitIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (implicitIntent.resolveActivity(packageManager) != null)
                startActivity(implicitIntent);
        }
    }

}
